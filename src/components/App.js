import React from "react";
import Page from "./Page";
import Topbar from "./Topbar";
import "antd/dist/antd.css";

const App = () => (
  <div
    style={{
      margin: 4,
      border: '1px solid grey',
      minHeight: '80vh',
      padding: 3,
    }}
  >
    <Topbar />
    <Page />
  </div>
);

export default App;
