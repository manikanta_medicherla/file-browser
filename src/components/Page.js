import React, { useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Typography, Button } from 'antd';
import { FolderOutlined, PlusOutlined, FileAddOutlined } from '@ant-design/icons';


import { navigateAction, createDirectoryAction } from "../actions";


const { Title } = Typography;

export default function Page() {
  const dispatch = useDispatch();
  const { rootDirectory, navigation } = useSelector((state) => {
    return {
      rootDirectory: state.rootDirectory,
      navigation: state.navigation
    };
  });
  const navigate = useCallback(
    (navigateTo) => dispatch(navigateAction(navigateTo, rootDirectory)),
    [dispatch, rootDirectory]
  );
  const { currentDirectory } = navigation || {};
  const { name: currentDirName, children = [] } = currentDirectory || {};
  const genNewDirName = () => (`New Folder ${(children || []).length} (in ${currentDirName})`)

  const create = useCallback(
    (parentDirName) => dispatch(createDirectoryAction(parentDirName, genNewDirName())),
    [dispatch, currentDirName]
  );
  return (
    <>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <Title level={4} style={{ margin: 0, padding: 0 }} strong>{currentDirName}</Title>
        <Button
          onClick={() => {
            create(currentDirName);
          }}
        >
          Create Dir <PlusOutlined />
        </Button>
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "column"
        }}
      >
        {children.map((item, itemIndex) => {

          return (
            <div
              key={itemIndex}
              onClick={() => {
                if (item.type === "folder") {
                  navigate(item.name);
                }
              }}
              style={{ display: "flex", border: '1px solid black', borderRight: 0, borderLeft: 0, padding: 4, margin: 3 }}
            >
              <div style={{ display: 'flex', alignItems: 'center' }}>
                {
                  item.type === "folder" ?
                    <FolderOutlined style={{ margin: 3, padding: 0 }} /> :
                    <FileAddOutlined style={{ margin: 3, padding: 0 }} />
                }
                <Title level={5} style={{ margin: 3, padding: 0 }} strong>{item.name}</Title>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
}
