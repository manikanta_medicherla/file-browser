import React, { useCallback, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Breadcrumb, Button } from 'antd';
import { navigateAction } from "../actions";
import { EnterOutlined } from '@ant-design/icons';



export default function Topbar() {
  const dispatch = useDispatch();
  const { rootDirectory, navigation } = useSelector((state) => {
    return {
      rootDirectory: state.rootDirectory,
      navigation: state.navigation
    };
  });

  const navigate = useCallback(
    (navigateTo) => dispatch(navigateAction(navigateTo, rootDirectory)),
    [dispatch, rootDirectory]
  );

  useEffect(() => {
    navigate('root')
  }, [])
  const {
    currentDirectory,
    meta: { message, traceBack = [] } = {}
  } = navigation || {};
  const { name: currentDirName, } = currentDirectory || {};

  return (
    <>
      {currentDirName !== "root" && (
        <EnterOutlined onClick={() => {
          debugger
          const tempArray = [...traceBack]
          tempArray.pop()
          const parentDirName = tempArray.pop()
          navigate(parentDirName);
        }} />
      )}
      {traceBack.map((item, index) => {
        return (

          <Breadcrumb.Item key={index}
            onClick={() => {
              navigate(item);
            }}>
            <Button type="text">
              {item}
            </Button>
          </Breadcrumb.Item>
        );
      })}

      <hr />

      <p style={{
        position: "fixed",
        bottom: 0,
        background: 'rgba(0,0,0,0.5)',
        padding: 2,
        borderRadius: '2px'
      }}>
        &nbsp;
        {currentDirName} - {message}
      </p>

    </>
  );
}
