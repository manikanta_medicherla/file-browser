export function navigateAction(navigateTo, rootDirectory) {
  return {
    type: "NAVIGATE",
    payload: { root: rootDirectory, navigateTo }
  };
}

export function createDirectoryAction(parentDirName, name) {
  return {
    type: "CREATEDIRECTORY",
    payload: { name, parentDirName }
  };
}
