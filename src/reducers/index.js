import { combineReducers } from "redux";
import { validateNavigation, createDir, root } from '../helpers'


export function rootDirectory(state = root, action) {
  switch (action.type) {
    case "CREATEDIRECTORY":
      const rootDirectory = state
      const {
        payload: { name = "New Folder", parentDirName }
      } = action || {};
      const newDirObj = { name, type: "folder", children: [] };

      const updatedRoot = createDir(rootDirectory, parentDirName, newDirObj)
      return updatedRoot;
    default:
      return state;
  }
}



export function navigation(state = { currentDirName: 'root' }, action) {

  switch (action.type) {
    case "NAVIGATE":
      const {
        payload: { navigateTo, root: rootDirectory }
      } = action || {};
      const [isValid, message, currentDirectory, traceBack] = validateNavigation(
        rootDirectory,
        navigateTo
      );
      // const updatedIndex = isValid ? navigationIndex : state.index;
      const navigationObj = {
        // currentDirName,
        currentDirectory: currentDirectory,
        meta: {
          traceBack,
          success: isValid,
          message
        }
      };
      return { ...state, ...navigationObj };
    default:
      return state;
  }
}


export default combineReducers({
  navigation,
  rootDirectory
});
