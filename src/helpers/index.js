export const root = {
    type: 'folder',
    name: 'root',
    children: [{
        "type": "folder",
        "name": "animals",
        "children": [
            {
                "type": "folder",
                "name": "cat",
                "children": [{
                    "type": "folder",
                    "name": "images",
                    "children": [
                        {
                            "type": "file",
                            "name": "cat001.jpg"
                        },
                        {
                            "type": "file",
                            "name": "cat002.jpg"
                        }]
                },]
            },
        ]
    }]
}
// export function getDir(rootDirectory, dirName) {
//     const root = { ...rootDirectory }

//     let pathTrace = []
//     function findDir(dirName, directory) {
//         pathTrace.push(directory.name)
//         if (directory.name == dirName) {
//             return directory
//         } else {
//             if (directory.children) {
//                 for (var i = 0; i < directory.children.length; i++) {
//                     return findDir(dirName, directory.children[i]);
//                 }
//             }
//         }
//     }
//     return [findDir(dirName, root), pathTrace]
// }
// export function getDir(rootDirectory, dirName) {
//     const root = { ...rootDirectory }

//     let pathTrace = []
//     let parentInMemory
//     function findDir(dirName, directory) {
//         pathTrace.push(directory.name)
//         parentInMemory = directory
//         if (directory.name == dirName) {
//             return directory
//         } else {
//             debugger
//             if (directory.children) {
//                 for (var i = 0; i < directory.children.length; i++) {
//                     return findDir(dirName, directory.children[i]);
//                 }
//             } else {
//                 return findDir(dirName, parentInMemory);
//             }
//         }
//     }
//     return [findDir(dirName, root), pathTrace]
// }
export function getDir(rootDirectory, dirName) {
    let pathTrace = []
    function searchTree(element, matchingTitle) {
        if (element && element.name) {
            pathTrace.push(element.name)
        }
        if (element.name === matchingTitle) {
            return element;
        } else if (element.children != null) {
            var i;
            var result = null;
            for (i = 0; result == null && i < element.children.length; i++) {
                result = searchTree(element.children[i], matchingTitle);
            }
            if (result == null) {
                pathTrace.pop()
            }
            return result;
        }
        pathTrace.pop()
        return null;
    }
    const result = searchTree(rootDirectory, dirName)
    return [result, pathTrace]
}
// export function createDir(rootDirectory, parentDirName, newDirObj) {
//     const root = { ...rootDirectory }
//     function findDir(dirName, newDirObj, directory) {
//         if (directory.name == dirName) {
//             directory.children.push(newDirObj);
//         } else {
//             for (var i = 0; i < directory.children.length; i++) {
//                 findDir(dirName, newDirObj, directory.children[i]);
//             }
//         }
//     }
//     findDir(parentDirName, newDirObj, root);
//     return root
// }

export function createDir(rootDirectory, parentDirName, newDirObj) {
    const root = { ...rootDirectory }
    function searchTree(element, matchingTitle) {
        if (element.name === matchingTitle) {
            element.children.push(newDirObj);
            return element;
        } else if (element.children != null) {
            var i;
            var result = null;
            for (i = 0; result == null && i < element.children.length; i++) {
                result = searchTree(element.children[i], matchingTitle);
            }
            return result;
        }
        return null;
    }
    searchTree(root, parentDirName);
    return root
}

export function validateNavigation(rootDirectory, dirName) {
    if (!dirName) return [false, "Navigation failed"];
    try {
        const [currentDirectory, traceBack] = getDir(rootDirectory, dirName)
        const message = currentDirectory
            ? "Navigation succeeded."
            : "Directory not found, navigation failed.";
        return [true, message, currentDirectory, traceBack]

    }
    catch (error) {
        debugger
        return [false, "Failed to navigate" + error.message];
    }
}

